The central mission of InterCoast Colleges is to provide associates degrees and certificate programs for careers in allied health, business, and skilled trade industries and prepare students to meet employer expectations for entry level employment.

Address : 2235 East Garvey Avenue North, West Covina, CA 91791

Phone : 626-337-6800